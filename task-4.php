<?php
/**
 * @param $number1
 * @param $number2
 * @return string
 */
function sum(string $number1, string $number2): string
{
    $length = strlen($number1) > strlen($number2) ? strlen($number1) : strlen($number2);
 
    // если числа короткие, дополняем нулями слева при необходимости
    $number1 = str_pad($number1, $length, '0', STR_PAD_LEFT);
    $number2 = str_pad($number2, $length, '0', STR_PAD_LEFT);
 	
 	// разбиваем строки в массивы и переворачиваем их
    $arr1 = array_reverse(str_split($number1));
    $arr2 = array_reverse(str_split($number2));
 
    for ($i = 0; $i < $length; $i++) {
        $arr2[$i] += $arr1[$i]; // суммируем последние разряды чисел
        $arr2[$i + 1] += (int)($arr2[$i] / 10); // если есть разряд для переноса, переносим его в следующий разряд
        $arr2[$i] %= 10; // и отсекаем его
    }
 
 	// убираем старший разряд, если он пустой
    if ($arr2[$length] == 0) {
        array_pop($arr2);
    }
 	

    // итоговый массив переворачиваем и объединяем в строку
    return implode(array_reverse($arr2), '');
}

echo sum('19', '11'); // 30
echo sum('999', '99'); // 1098